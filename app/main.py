from fastapi import FastAPI
import os
import redis

app = FastAPI()


@app.get("/version")
def version():
    return {"version": 12}

@app.get("/")
def read_root():
    return {"Hello": "Hacker"}


@app.get("/high-load/{iter}")
def high_load(iter: int = 10000):
    for i in range(iter):
        iter * iter

@app.get("/redis")
def test_redis():
    redis_host = os.getenv('REDIS_HOST', 'unknown_redis_host')
    redis_port = int(os.getenv('REDIS_PORT', '6379'))
    res = {'redis_host': redis_host,
           'redis_port': redis_port}


    r = redis.StrictRedis(host=redis_host, port=redis_port)
    res['redis_connect'] = 'ok'
    r.set('a', 'abc')
    res['redis_set'] = 'ok'
    raw = r.get('a')
    res['got_raw'] = 'ok'
    v = raw.decode('utf-8')
    res['decode'] = 'ok'
    res['v'] = v

    return res


'''
class RedisDb:

    def __init__(self):
        self.redis_db_host = 'moremmr-cache.redis.cache.windows.net'
        self.redis_db_port = 6380
        self.redis_db_password = 'tds9zrSQg+4MyCJXSZQtWEa0Arq4+duuchFL+UbeHbg='
        self.redis_db = None

    def connect_to_redis_db(self, reconnect=False):
        if type(self.redis_db).__name__ != 'StrictRedis' or reconnect:
            try:
                self.redis_db = redis.StrictRedis(host=self.redis_db_host,
                                                  port=self.redis_db_port,
                                                  db=0,
                                                  password=self.redis_db_password,
                                                  ssl=True,
                                                  socket_connect_timeout=10,
                                                  retry_on_timeout=True)
            except Exception as ex:
                print('Encountered exception in method {0}.{1}:'.format('RedisDb', 'connect_to_redis_db'))
                print(ex)

                return False

        return True

    def set_expire(self, name, expiration_seconds = 0):
        if expiration_seconds > 0:
            self.redis_db.expire(name, expiration_seconds)

    def get_hash(self, name, attribute):
        return self.redis_db.hget(name, attribute)

    def set_hash(self, name, attribute, value, expiration_seconds=0):
        self.redis_db.hset(name, attribute, value)
        self.set_expire(name, expiration_seconds)

    def queue_push(self, name, value):
        self.redis_db.rpush(name, value)

    def queue_pop(self, name):
        return self.redis_db.lpop(name).decode("utf-8")

    def increment(self, name, value, expiration_seconds = 0):
        incr_result = self.redis_db.incr(name, value)
        self.set_expire(name, expiration_seconds)
        return incr_result

    def get_value(self, name):
        return self.redis_db.get(name)

    def set_value(self, name, value, expiration_seconds = 0):
        self.redis_db.set(name, value)
        self.set_expire(name, expiration_seconds)

    def delete_value(self, key):
        self.redis_db.delete(key)

    def get_len(self, name):
        return self.redis_db.llen(name)

    def exists(self, name):
        return self.redis_db.exists(name)

    def get_list_of_set_values(self, name):
        values_list = []
        members = self.redis_db.smembers(name)
        for member in members:
            if member is not None:
                values_list.append(member.decode("utf-8"))
        return values_list

    def put_list_to_set(self, name, values_list = [], expiration_seconds=0):
        for value in values_list:
            self.redis_db.sadd(name, value)
        self.set_expire(name, expiration_seconds)

    def pop_random_values_from_set(self, name, values_count = 0):
        values_list = []
        for i in range(0, values_count):
            set_value = self.redis_db.spop(name)
            if set_value is not None:
                values_list.append(int(set_value.decode("utf-8")))
        return values_list

    def get_set_cardinality(self, name):
        return self.redis_db.scard(name)

    def get_pipeline(self):
        return self.redis_db.pipeline()

    def get_all_hash_attributes_values(self, name):
        return self.redis_db.hscan(name)

    def get_ttl(self, name):
        return self.redis_db.ttl(name)

'''
